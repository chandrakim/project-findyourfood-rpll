package com.tubesandroid.activity;

        import android.app.AlertDialog;
        import android.content.ContentValues;
        import android.content.Context;
        import android.content.DialogInterface;
        import android.content.SharedPreferences;
        import android.graphics.Bitmap;
        import android.graphics.BitmapFactory;
        import android.graphics.drawable.BitmapDrawable;
        import android.graphics.drawable.Drawable;
        import android.text.TextUtils;
        import android.util.Log;
        import android.widget.EditText;
        import android.widget.Toast;

        import java.io.IOException;
        import java.io.InputStream;
        import java.net.HttpURLConnection;
        import java.net.URL;
        import java.net.URLConnection;
        import java.util.regex.Matcher;
        import java.util.regex.Pattern;

/**
 * Created by itomanu on 6/4/2015.
 * Modified by PCK
 */
public class Helper {

    /** TOAST & DIALOG **/

    public static void showToast(Context context, String message, int length) {
        Toast.makeText(context, message, length).show();
    }

    public static void showToast(Context context, String message) {
        showToast(context, message, Toast.LENGTH_SHORT);
    }

    public static void showDialog(Context context, String title, String message, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", listener)
                .show();
    }

    public static void showYesNoDialog(Context context, String title, String message, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Yes", listener)
                .setNegativeButton("No", listener)
                .show();
    }

    public static void showYesNoDialog(Context context, String message, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder
                .setMessage(message)
                .setPositiveButton("Yes", listener)
                .setNegativeButton("No", listener)
                .show();
    }

    /** END OF TOAST & DIALOG **/



    /** SHARED PREFERENCES **/
    public static final String PREFS_NAME = "PrefsFile";

    public static void setCookie(Context context, String key, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static Object getCookie(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getString(key, null);
    }


    public static InputStream openHttpConnection(String urlString) throws IOException {
        InputStream in = null;
        int response = -1;
        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();

        if(!(conn instanceof HttpURLConnection))
            throw new IOException("Not an HTTP Connection");

        try {
            HttpURLConnection httpConn = (HttpURLConnection) conn;

            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();
            response = httpConn.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK){
                in = httpConn.getInputStream();
            }
        }catch(Exception ex){
            Log.d("Networking", ex.getLocalizedMessage());
            throw new IOException("Error connecting");

        }
        return in;


    }

    public static Drawable downloadImage(Context context, String urlString){
        InputStream in;
        BitmapDrawable drawable = null;

        try{
            in = openHttpConnection(urlString);

            Bitmap bMap = BitmapFactory.decodeStream(in);
            if(in != null){
                in.close();
            }
            drawable = new BitmapDrawable(context.getResources());

            return drawable;
        }catch (Exception e){
            Log.e("Error reading file",e.toString());
            return null;
        }

    }

    /** END OF SHARED PREFERENCES **/
}
