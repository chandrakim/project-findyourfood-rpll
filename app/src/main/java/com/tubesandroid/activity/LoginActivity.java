package com.tubesandroid.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.tubesandroid.R;
import com.tubesandroid.helper.StudioHttpClient;
import com.tubesandroid.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;


public class LoginActivity extends Activity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, DialogInterface.OnClickListener {

    public static final String USER_KEY = "user_email";
    EditText etEmail, etPassword;
    CheckBox cbShowPass;
    Button btLogin, btRegister;
    Activity self;
    JSONObject jsonobject;
    JSONArray jsonarray;
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        String url = Constants.SERVER_URL + "resto";
        // init;
        self        = this;
        etEmail     = (EditText) findViewById(R.id.main_et_email);
        etPassword  = (EditText) findViewById(R.id.main_et_password);
        cbShowPass  = (CheckBox) findViewById(R.id.main_cb_showpass);
        btLogin     = (Button) findViewById(R.id.main_bt_login);
        btRegister  = (Button) findViewById(R.id.main_bt_register);

        // set event listener;
        cbShowPass.setOnCheckedChangeListener(this);
        btLogin.setOnClickListener(this);
        btRegister.setOnClickListener(this);
        if (Helper.getCookie(self,USER_KEY)!=null){
            goToMain();
        }


    }

    // DownloadJSON AsyncTask
    private class DownloadJSON extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(LoginActivity.this);
            // Set progressdialog title
            mProgressDialog.setTitle("Login User");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }


        @Override
        protected String doInBackground(String... params) {
            try {
                // Retrieve JSON Objects from the given URL address
                String username = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                //jsonarray = JSONFunction.getJSONfromURL("http://findyourfood.herokuapp.com/users?username=" + username + "&?password=" + password);


//            a.addHeader("Content-Type","application/json");
                StudioHttpClient a = new StudioHttpClient("http://findyourfood-rpll.herokuapp.com/users/login");
                //StudioHttpClient a = new StudioHttpClient("localhost:2403/users/login");
                a.addParam("username", username);
                a.addParam("password", password);
                Log.d("asdf","username = "+username+" password = "+password);
                a.execute(StudioHttpClient.RequestMethod.POST);
                if (a.getResponseCode()==200){
                    Log.d("aaa",a.getResponse());
                    Helper.setCookie(self,USER_KEY,username);
                    return a.getResponse();
                }else {
                    Log.d("ccc",""+a.getResponseCode());
                    Log.d("bbb",a.getErrorMessage());
                    return null;
                }
            } catch (Exception e) {
                Log.d("abc","abc");
                e.printStackTrace();
                return null;
            }
        }



        @Override
        protected void onPostExecute(String result){
            //Log.d("result",result);
            mProgressDialog.hide();
            if (result==null){
                Toast.makeText(getApplicationContext(), "Username or Password Error",
                        Toast.LENGTH_SHORT).show();

            }else {
                goToMain();
            }

        }

    }

    public boolean checkUser(){

        boolean result=false;


        if (jsonarray!=null){
            result=true;
        }else result=false;


        return result;
    }




    public void goToMain() {
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.main_cb_showpass) {
            if(isChecked) {
                etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            } else {
                etPassword.setInputType(129);
            }
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;

        switch (v.getId()) {
            case R.id.main_bt_login:{
                if (Helper.getCookie(self,USER_KEY)!=null){
                    goToMain();
                }else {
                    new DownloadJSON().execute();
                }


            }

            break;

            case R.id.main_bt_register:
                intent = new Intent(getBaseContext(), RegisterActivity.class);
                startActivityForResult(intent, 1);
                break;
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                break;
        }
    }
}
