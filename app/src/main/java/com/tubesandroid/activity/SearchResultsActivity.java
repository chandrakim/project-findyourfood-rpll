package com.tubesandroid.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.app.SearchManager;
import android.content.Intent;

import com.tubesandroid.helper.JSONFunction;
import com.tubesandroid.R;
import com.tubesandroid.fragment.ListAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;

public class SearchResultsActivity extends ActionBarActivity {
    Activity self;
    JSONObject jsonobject;
    JSONArray jsonarray;
    ListView lv;
    ProgressDialog mProgressDialog;
    private TextView txtQuery;
    private String query;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        // get the action bar
        ActionBar actionBar = getActionBar();

        // Enabling Back navigation on Action Bar icon
        lv = (ListView) findViewById(R.id.listView);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);

        new DownloadJSON().execute();

        txtQuery = (TextView) findViewById(R.id.txtQuery);
        txtQuery.setEnabled(false);
        handleIntent(getIntent());
    }
    // DownloadJSON AsyncTask
    private class DownloadJSON extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(getApplicationContext());
            // Set progressdialog title
            mProgressDialog.setTitle("Loading Content");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            progressBar.setVisibility(View.VISIBLE);
           //mProgressDialog.show();
        }


        @Override
        protected Void doInBackground(Void... params) {
            // Create an array
            // Retrieve JSON Objects from the given URL address

            String link = null;
            try {
                link = "http://findyourfood-rpll.herokuapp.com/resto?dish="+URLEncoder.encode(query, "UTF-8");
                Log.i("###",link);
                jsonarray = JSONFunction
                        .getJSONfromURL(link);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            //Log.d("ASDQ",""+jsonarray.length());
            mProgressDialog.hide();
            progressBar.setVisibility(View.GONE);
            if (jsonarray.length() == 0){
                txtQuery.setText("No Results: " + query);
                txtQuery.setEnabled(true);
            }else{
                ListAdapter adapter = new ListAdapter(getApplicationContext(), jsonarray);
                lv.setAdapter(adapter);
            }



        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.


        getMenuInflater().inflate(R.menu.menu_search_results, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    /**
     * Handling intent data
     */
    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            query = intent.getStringExtra(SearchManager.QUERY);

            /**
             * Use this query to display search results like
             * 1. Getting the data from SQLite and showing in listview
             * 2. Making webrequest and displaying the data
             * For now we just display the query only
             */


        }



    }
}
