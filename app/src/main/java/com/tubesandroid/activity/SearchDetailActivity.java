package com.tubesandroid.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.tubesandroid.helper.JSONFunction;
import com.tubesandroid.R;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;


public class SearchDetailActivity extends ActionBarActivity {
    RatingBar ratingBar;
    Button button,buttonAdd;
    private TextView textView;
    ProgressDialog mProgressDialog;
    Activity self;

    JSONObject jsonobject;
    JSONArray jsonarray,jsonarray2;
    ListView lv;
    String name=null,address=null,facility=null;
    double longitude=0;
    double latitude=0;
    //LayoutInflater inflater;
    //ViewGroup container;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        self=this;
        setContentView(R.layout.activity_search_detail);
        new DownloadJSON().execute();
        textView = (TextView) findViewById(R.id.textView);
        //View view = inflater.inflate(R.layout.activity_search_detail, container, false);
        lv = (ListView) findViewById(R.id.listView);

        handleIntent(getIntent());

        ratingBar=(RatingBar)findViewById(R.id.ratingBar);

        buttonAdd=(Button)findViewById(R.id.moreCommentButton);
        buttonAdd.setEnabled(false);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), AddCommentActivity.class);
                intent.putExtra("namaResto",name);

                startActivity(intent);

            }
        });



        Intent in=getIntent();

        int rating=0;
        String p=in.getStringExtra("position");
        int position=Integer.parseInt(p);
        String image=null;
        ImageView i=(ImageView)findViewById(R.id.imageView);
        try {
            JSONObject o= com.tubesandroid.fragment.ListAdapter.data.getJSONObject(position);
            address =(String) o.get("alamat");
            name=(String)o.get("namaResto");
            rating=Integer.parseInt(o.get("avgRating").toString());
            facility=(String)o.get("fasilitas");
            image=(String)o.get("url");
            longitude=Double.parseDouble(o.get("longitude").toString());
            latitude=Double.parseDouble(o.get("latitude").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Ion.with(i)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.error)
                .load(image);

        TextView tv1=(TextView)findViewById(R.id.textView);
        TextView tv2=(TextView)findViewById(R.id.textView5);
        TextView tv3=(TextView)findViewById(R.id.textView2);
        tv2.setText(address);
        tv1.setText(name);
        tv3.setText(facility);
        ratingBar.setRating(rating);

        button=(Button)findViewById(R.id.directionButton);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getBaseContext(),MapsActivity.class);
//
//
//                startActivity(intent);

                // Create a Uri from an intent string. Use the result to create an Intent.
                //Uri gmmIntentUri = Uri.parse("google.streetview:cbll=107.615575,-6.888844");
                //Uri gmmIntentUri = Uri.parse("geo:107.615575,-6.888844");
                Uri gmmIntentUri = Uri.parse("google.navigation:q="+latitude+","+longitude+"");
                // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                // Make the Intent explicit by setting the Google Maps package
                mapIntent.setPackage("com.google.android.apps.maps");

                // Attempt to start an activity that can handle the Intent
                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                }


            }
        });


    }

    // DownloadJSON AsyncTask
    private class DownloadJSON extends AsyncTask<Void, Void, Void> {

//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            // Create a progressdialog
//            //  mProgressDialog = new ProgressDialog(getActivity());
//            // Set progressdialog title
//            mProgressDialog.setTitle("Loading Content");
//            // Set progressdialog message
//            mProgressDialog.setMessage("Loading...");
//            mProgressDialog.setIndeterminate(false);
//            // Show progressdialog
//            mProgressDialog.show();
//        }


        @Override
        protected Void doInBackground(Void... params) {
            // Create an array
            // Retrieve JSON Objects from the given URL address

            String link = null,link2=null;
            String cookiesUsername = Helper.getCookie(self, LoginActivity.USER_KEY).toString();
            try {
                link = "http://findyourfood-rpll.herokuapp.com/review?namaResto="+URLEncoder.encode(name, "UTF-8");
                //  "//"+URLEncoder.encode("{\"$sort\":{\"waktu\":-1}}", "UTF-8");
                //+
                //link = "http://findyourfood.herokuapp.com/review?namaResto=Ceker%20Setan";
                Log.e("hehe123", cookiesUsername);
                link2 = "http://findyourfood-rpll.herokuapp.com/review?username="+cookiesUsername+"&namaResto="+URLEncoder.encode(name, "UTF-8");

                Log.e("link = ", link);
                Log.e("link2 = ", link2);

                System.out.println("Link JSON"+link);
                jsonarray = JSONFunction
                        .getJSONfromURL(link);
                jsonarray2 = JSONFunction
                        .getJSONfromURL(link2);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {

            if (jsonarray2 == null){
                buttonAdd.setEnabled(true);
            }

            //Log.d("ASDQ",""+jsonarray.length());
            //mProgressDialog.hide();

            if (jsonarray != null) {
                com.tubesandroid.fragment.ListAdapter3 adapter = new com.tubesandroid.fragment.ListAdapter3(self, jsonarray);
                lv.setAdapter(adapter);
            }
        }
    }


    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    /**
     * Handling intent data
     */
    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            /**
             * Use this query to display search results like
             * 1. Getting the data from SQLite and showing in listview
             * 2. Making webrequest and displaying the data
             * For now we just display the query only
             */
            textView.setText("Search Query: " + query);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        getMenuInflater().inflate(R.menu.menu_search_detail, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
