package com.tubesandroid.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tubesandroid.R;
import com.tubesandroid.ShakeEventManager;
import com.tubesandroid.helper.StudioHttpClient;

import java.text.DateFormat;
import java.util.Date;
import java.util.Random;


public class AddCommentActivity extends ActionBarActivity implements ShakeEventManager.ShakeListener{

    RatingBar ratingBar;
    Button button;
    TextView textViewComment;
    EditText et_comment;
    Activity self;
    int counter;
    private ShakeEventManager sd;
    ProgressDialog mProgressDialog;
    int angka;

    public void displayToast(String message){
        Toast.makeText(getBaseContext(), "user rate:" + message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_comment);
        counter=0;
        sd = new ShakeEventManager();
        sd.setListener(this);
        sd.init(this);
        self        = this;
        et_comment=(EditText)findViewById(R.id.editText);
        ratingBar=(RatingBar)findViewById(R.id.ratingBar2);
        button = (Button) findViewById(R.id.commentButton);

        button.setEnabled(false);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                displayToast("" + rating);
                counter++;
                if (!et_comment.getText().toString().equals("")) {
                    button.setEnabled(true);
                }
            }
        });

        et_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(counter==1 &&!et_comment.getText().toString().equals("")){
                    button.setEnabled(true);
                }
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DownloadJSON().execute();
            }
        });

    }

    @Override
    public void onShake() {
        Random r = new Random();
        angka = r.nextInt(80 - 65) + 65;
        Log.d("BCA","VASD");
    }
    @Override
    protected void onResume() {
        super.onResume();
        sd.register();
    }


    @Override
    protected void onPause() {
        super.onPause();
        sd.deregister();
    }

    private class DownloadJSON extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(self);
            // Set progressdialog title
            mProgressDialog.setTitle("Posting your review");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            Intent in = getIntent();
            final String review = et_comment.getText().toString();
            final String username = Helper.getCookie(self, LoginActivity.USER_KEY).toString();
            final String namaResto=in.getStringExtra("namaResto");
            float rating = ratingBar.getRating();
            final String rate = String.valueOf(rating);
            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());

            try {
                StudioHttpClient a = new StudioHttpClient("http://findyourfood-rpll.herokuapp.com/review");
                a.addParam("username", username);
                a.addParam("namaResto",namaResto);
                a.addParam("rating", rate);
                a.addParam("review", review);
                a.addParam("waktu",currentDateTimeString);

                a.execute(StudioHttpClient.RequestMethod.POST);

                if (a.getResponseCode() == 200) {
                    Log.d("aaa", "" + a.getResponseCode());
                    return a.getResponse();

                } else {
                    Log.d("ccc", "" + a.getResponseCode());
                    return null;
                }

            } catch (Exception e) {
                Log.d("abc", "abc");
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result == null) {
                Toast.makeText(getApplicationContext(), "Username or Password Already Exist",
                        Toast.LENGTH_SHORT).show();

            }
            else{
                Toast.makeText(self, "Comment berhasil", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_comment, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
