package com.tubesandroid.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.tubesandroid.R;
import com.tubesandroid.helper.StudioHttpClient;
import com.tubesandroid.utils.GMailSender;


public class
        RegisterActivity extends Activity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {
    EditText etEmail, etPassword, etName, etAddress;
    CheckBox cbShowPass;
    Spinner spGender;
    Button btRegister;
    Activity self;
    public static final String USER_KEY = "user_email";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // init;
        self        = this;
        etEmail     = (EditText) findViewById(R.id.register_et_email);
        etPassword  = (EditText) findViewById(R.id.register_et_password);
        cbShowPass  = (CheckBox) findViewById(R.id.register_cb_showpass);


        btRegister  = (Button) findViewById(R.id.register_bt_register);

        // set event listener;
        cbShowPass.setOnCheckedChangeListener(this);
        btRegister.setOnClickListener(this);
    }

    public void doRegister() {
        String email = etEmail.getText().toString();
        String pass = etPassword.getText().toString();


        if(email != null && email.equals("")){
            etEmail.setError("Email is Required");
        }

        if(pass.length() < 3){
            etPassword.setError("Password must be more than 3");
        }



        goToMain();
    }

    // DownloadJSON AsyncTask
    private class DownloadJSON extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                // Retrieve JSON Objects from the given URL address
                String username = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                //jsonarray = JSONFunction.getJSONfromURL("http://findyourfood.herokuapp.com/users?username=" + username + "&?password=" + password);
                //Log.d("asdf","username = "+username+" password = "+password);

                StudioHttpClient a = new StudioHttpClient("http://findyourfood-rpll.herokuapp.com/users");
               // StudioHttpClient a = new StudioHttpClient("localhost:2403/users");
//            a.addHeader("Content-Type","application/json");
                a.addParam("username", username);
                a.addParam("password", password);
                //a.addParam("name", password);

                a.execute(StudioHttpClient.RequestMethod.POST);
                if (a.getResponseCode()==200){
                    Log.d("aaa",a.getResponse());
                    Helper.setCookie(self,USER_KEY,username);
                    return a.getResponse();
                }else {
                    Log.d("ccc",""+a.getResponseCode());
                    Log.d("bbb",a.getErrorMessage());
                    return null;
                }
            } catch (Exception e) {
                Log.d("abc","abc");
                e.printStackTrace();
                return null;
            }
        }


        @Override
        protected void onPostExecute(String result){
            //Log.d("result",result);
            if (result==null){
                Toast.makeText(getApplicationContext(), "Username or Password Already Exist",
                        Toast.LENGTH_SHORT).show();

            }else {

                try {
                    GMailSender sender = new GMailSender("nendra.t.w@gmail.com", "@ptx4869*0-");
                    sender.sendMail("This is Subject",
                            "This is Body",
                            "nendra.t.w@gmail.com",
                            "chandra.welim@gmail.com");
                } catch (Exception e) {
                    Toast.makeText(RegisterActivity.this, "There was a problem sending the email.", Toast.LENGTH_LONG).show();
                }

                goToMain();

//                Intent intent = new Intent(getBaseContext(), CodeVerifyActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
            }
        }
    }


    public void goToMain() {
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.register_cb_showpass) {
            if(isChecked) {
                etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            } else {
                etPassword.setInputType(129);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.register_bt_register) {
            new DownloadJSON().execute();
            //doRegister();
        }
    }
}
