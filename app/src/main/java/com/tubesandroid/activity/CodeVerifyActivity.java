package com.tubesandroid.activity;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tubesandroid.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CodeVerifyActivity extends ActionBarActivity {


    @Bind(R.id.lblVerification) protected TextView lblVerification;
    @Bind(R.id.txtCode) protected EditText txtCode;
    @Bind(R.id.btnCode) protected Button btnCode;

    String kode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_verify);
        ButterKnife.bind(this);


        btnCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(txtCode.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Please insert the Verification Code",
                            Toast.LENGTH_SHORT).show();
                }else if(!txtCode.getText().toString().equals(kode)){
                    Toast.makeText(getApplicationContext(), "Verification Code is wrong !",
                            Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(getBaseContext(),MainActivity.class);

                    startActivity(intent);
                }


            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_code_verify, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
