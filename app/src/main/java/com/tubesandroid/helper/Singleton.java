package com.tubesandroid.helper;

/**
 * Created by PONG on 6/30/2015.
 */
public class Singleton {

    public static String sessionUsername;
    public static String sessionResto;


    public String getSessionUsername() {
        return sessionUsername;
    }

    public String getSessionResto() {
        return sessionResto;
    }

    public void setSessionUsername(String sessionUsername) {
        this.sessionUsername = sessionUsername;
    }

    public void setSessionResto(String sessionResto) {
        this.sessionResto = sessionResto;
    }
}
