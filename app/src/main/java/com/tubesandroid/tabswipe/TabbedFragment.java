package com.tubesandroid.tabswipe;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tubesandroid.R;
import com.tubesandroid.fragment.TopRestoFragment;
import com.tubesandroid.fragment.TopReviewFragment;

public class TabbedFragment extends Fragment {
    public static final String TAG = TabbedFragment.class.getSimpleName();

    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;

    public static TabbedFragment newInstance() {
        return new TabbedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tabbed, container, false);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        mViewPager = (ViewPager) v.findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);



        return v;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
//            Fragment fragment = new TabbedContentFragment();
//            Bundle args = new Bundle();
//            args.putInt(TabbedContentFragment.ARG_SECTION_NUMBER, position + 1);
//            fragment.setArguments(args);
//            return fragment;
            switch (position){
                case 0 :
                    TopRestoFragment topRestoFragment = new TopRestoFragment();
                    return  topRestoFragment;
                case 1:
                    TopReviewFragment topReviewFragment = new TopReviewFragment();
                    return topReviewFragment;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "Top Rating";
                case 1:
                    return "Top Review";
            }
            return null;
        }
    }
}
