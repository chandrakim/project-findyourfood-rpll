package com.tubesandroid.fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by kevin on 7/1/2015.
 */
public class ListAdapterSearchDetail extends BaseAdapter{
    Context context;
    public static JSONArray data;
    LayoutInflater layoutInflater;
    ListView lv;

    public ListAdapterSearchDetail(Context context, JSONArray data,ListView lv) {
        this.context = context;
        this.data = data;
        this.layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.lv=lv;
    }

    @Override
    public int getCount() {
        return data.length();
    }

    @Override
    public Object getItem(int position) {
        int i=0;
        try {
            i= (int) data.get(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return i;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
