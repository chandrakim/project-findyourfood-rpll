package com.tubesandroid.fragment;

/**
 * Created by PONG on 7/2/2015.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.tubesandroid.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ListAdapter3 extends BaseAdapter {

    Context context;
    public static JSONArray data;
    LayoutInflater layoutInflater;

    public ListAdapter3(Context context, JSONArray data) {
        System.out.println("array yg masuk = "+data.length());
        this.context = context;
        this.data = data;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //System.out.println("UdahMasukKok");
    }

    @Override
    public int getCount() {
        return data.length();
    }

    @Override
    public Object getItem(int position) {
        int i=0;
        try {
            i= (int) data.get(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return i;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String text=null,image=null, review=null,username=null;
        double rating=0;
        final int post=position;
        View view=convertView;
        view=layoutInflater.inflate(R.layout.listview_detail_search_item,null);
        //ImageView i=(ImageView)view.findViewById(R.id.img);

        try {

            JSONObject o=data.getJSONObject(position);

            username = (String) o.get("username");
            //text =(String) o.get("namaResto");
            rating=Double.parseDouble(o.get("rating").toString());
            review = (String) o.get("review");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Ion.with(i).placeholder(R.drawable.placeholder).error(R.drawable.error).load(image);
        TextView tv_username = (TextView)view.findViewById(R.id.username);
        //TextView tv_name=(TextView)view.findViewById(R.id.restoName);
        RatingBar rb=(RatingBar)view.findViewById(R.id.restoRating);
        TextView tv_review = (TextView)view.findViewById(R.id.review);

        //tv_name.setText(text);
        rb.setRating((float)rating);
        tv_username.setText(username);
        tv_review.setText(review);



        rb.setEnabled(false);
//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String p = String.valueOf(post);
//                Intent in = new Intent(context, SearchDetailActivity.class);
//
//
//                in.putExtra("position", p);
//                context.startActivity(in);
//            }
//        });

        return view;
    }


}

