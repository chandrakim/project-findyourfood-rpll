package com.tubesandroid.fragment;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.tubesandroid.R;
import com.tubesandroid.activity.SearchDetailActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by kevin on 7/1/2015.
 */
public class ListAdapter2 extends BaseAdapter {
    Context context;
    public static JSONArray data;
    LayoutInflater layoutInflater;

    public ListAdapter2(Context context, JSONArray data) {
        this.context = context;
        this.data = data;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.length();
    }

    @Override
    public Object getItem(int position) {
        int i=0;
        try {
            i= (int) data.get(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return i;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String text=null,image=null;
        int rating=0;
        final int post=position;
        View view=convertView;
        view=layoutInflater.inflate(R.layout.listview_item,null);
        ImageView i=(ImageView)view.findViewById(R.id.img);

        try {
            JSONObject o=data.getJSONObject(position);
            text =(String) o.get("namaResto");
            rating=Integer.parseInt(o.get("avgRating").toString());
            image=(String)o.get("url");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Ion.with(i)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.error)
                .load(image);
        TextView tv_name=(TextView)view.findViewById(R.id.restoName);
        RatingBar rb=(RatingBar)view.findViewById(R.id.restoRating);
        tv_name.setText(text);
        rb.setRating(rating);
        rb.setEnabled(false);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String p = String.valueOf(post);
                Intent in = new Intent(context, SearchDetailActivity.class);

                System.out.println("ASD");

                in.putExtra("position", p);
                context.startActivity(in);
            }
        });

        return view;
    }


}
