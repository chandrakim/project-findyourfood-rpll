package com.tubesandroid.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tubesandroid.helper.JSONFunction;
import com.tubesandroid.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class TopReviewFragment extends Fragment {

    Activity self;

    JSONObject jsonobject;
    JSONArray jsonarray;
    ListView lv;
    ProgressDialog mProgressDialog;
    ProgressBar progressBar;
    TextView txtQuery;

    public static TopReviewFragment newInstance(String param1, String param2) {
        TopReviewFragment fragment = new TopReviewFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    public TopReviewFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_top_review, container, false);
        lv = (ListView) view.findViewById(R.id.listView2);
        txtQuery = (TextView) view.findViewById(R.id.txtQuery);
        txtQuery.setEnabled(false);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        self = getActivity();
        new DownloadJSON().execute();
        return view;
    }

    // DownloadJSON AsyncTask
    private class DownloadJSON extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(getActivity());
            // Set progressdialog title
            mProgressDialog.setTitle("Loading Content");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
         // mProgressDialog.show();
            progressBar.setVisibility(View.VISIBLE);
        }


        @Override
        protected Void doInBackground(Void... params) {
            // Create an array
            // Retrieve JSON Objects from the given URL address

            String link = null;
            try {
                link = "http://findyourfood-rpll.herokuapp.com/resto?" + URLEncoder.encode("{\"$sort\":{\"review\":-1},\"$limit\":\"5\"}", "UTF-8");
                jsonarray = JSONFunction
                        .getJSONfromURL(link);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void args) {


            Log.i("top review length", "" + jsonarray.length());
            Log.i("top review string", "" + jsonarray.toString());
            mProgressDialog.hide();
            progressBar.setVisibility(View.GONE);
            if (jsonarray.length()!= 0) {
                ListAdapter2 adapter2 = new ListAdapter2(self, jsonarray);
                lv.setAdapter(adapter2);

            } else{
                txtQuery.setText("No Results");

                txtQuery.setEnabled(true);

            }


        }

    }
}
