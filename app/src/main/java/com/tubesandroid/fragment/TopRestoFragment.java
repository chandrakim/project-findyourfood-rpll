package com.tubesandroid.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tubesandroid.helper.JSONFunction;
import com.tubesandroid.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;

public class TopRestoFragment extends Fragment {
    Activity self;

    JSONObject jsonobject;
    public static JSONArray jsonarray;
    ListView lv;
    ProgressDialog mProgressDialog;
    ProgressBar progressBar;
    TextView txtQuery;

    public static TopRestoFragment newInstance(String param1, String param2) {
        TopRestoFragment fragment = new TopRestoFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    public TopRestoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_top_resto, container, false);
        lv = (ListView) view.findViewById(R.id.listView);
        txtQuery = (TextView) view.findViewById(R.id.txtQuery);
        txtQuery.setEnabled(false);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        self = getActivity();
        new DownloadJSON().execute();
        return view;
    }

    // DownloadJSON AsyncTask
    private class DownloadJSON extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(getActivity());
            // Set progressdialog title
            mProgressDialog.setTitle("Loading Content");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            // mProgressDialog.show();
            progressBar.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            // Create an array
            // Retrieve JSON Objects from the given URL address

            String link = null;
            try {
                String encode =
                link = "http://findyourfood-rpll.herokuapp.com/resto?"+ URLEncoder.encode("{\"$sort\":{\"avgRating\":-1},\"$limit\":\"5\"}", "UTF-8");
                Log.e("###",link);
                jsonarray = JSONFunction
                        .getJSONfromURL(link);
                Log.e("###", "try");
            } catch (/*UnsupportedEncodingException*/ Exception e) {
                Log.e("###", "catch");
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            Log.e("ASDQ",""+jsonarray.length());
            mProgressDialog.hide();
//
            progressBar.setVisibility(View.GONE);
            if (jsonarray.length() != 0) {
                ListAdapter adapter = new ListAdapter(self, jsonarray);
                lv.setAdapter(adapter);

            } else{
                txtQuery.setText("No Results");
                txtQuery.setEnabled(true);

            }
        }
    }
}

